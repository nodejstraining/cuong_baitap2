 var express = require('express');
 var router = express.Router();
 var models = require('../models/person');
 var mongoose = require("mongoose");
 router.route('/')
  .get(function(req, res){
    console.log(req.query.id);
    res.render('person')
  })
  .post(function(req,res){

    models.Person({
  		name : req.body.name,
  		phone_number : req.body.phonenumber.toString(),
  		city : req.body.city
  	}).save(function(error){
  	if(error) res.send("error")
  		else res.redirect("/api/person");      
  	})
  })
  router.route('/delperson').get(function(req, res){
  models.Person.findOneAndRemove({name:"BaoCuong"}, function(err){
    if(err) throw err;
    else{
      res.redirect('/api/person');
    }
  });
  });

  router.route('/listperson').get(function(req, res){

  models.Person.find({}, function(err, result){

    if(err){
      console.log(err);
      res.send("can't find record");
    }else{
      console.log(result);
      res.render('listperson', {result_person:JSON.stringify(result)});

     }
    });
   
  });
//delete
  router.route('/del_id').get(function(req,res){
    models.Person.findOneAndRemove({_id: req.query.id}, function(err){
    if(err) throw err;
    else{
      res.redirect('/api/person/listperson');
    }
  });
  })

//update
  router.get('/update',function(req,res){
    var id_update = req.query.id;
     console.log(req.query.id);
    console.log(id_update);
    console.log('something here');
       res.render('updateperson',{my_id: id_update});
  });
  router.post('/update',function(req,res){
    
    var newname = req.body.newname;
    var newphone = req.body.newphone;
    var newcity = req.body.newcity;
    console.log(req.query.id);
    var use_id = req.query.id;
    models.Person.findOneAndUpdate({_id: use_id},{name: newname,phone_number: newphone,
      city: newcity},function(err, user){
      if(err) throw err;
      res.redirect('/api/person/listperson');
  });
  });



 module.exports = router;